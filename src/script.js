var values = {
  volume : 0,
  root : {
    freq : 240,
    listen : false
  },
  interval : {
    dividend : 3,
    divisor : 2,
    listen : false
  },
  getIntervalFreq : function(){
    let ratio = this.interval.dividend / this.interval.divisor
    return this.root.freq * ratio
  }
}
var waveform = new Tone.Waveform(512)
var mixer = new Tone.Add().fan(waveform, Tone.Master)
var rmult = new Tone.Multiply().connect(mixer, 0, 0)
var imult = new Tone.Multiply().connect(mixer, 0, 1)
var rs = new Tone.Signal(0.5).connect(rmult, 0, 0)
var is = new Tone.Signal(0.5).connect(imult, 0, 0)
var root = new Tone.Oscillator(values.root.freq, "sine").connect(rmult, 0, 1)
var interval = new Tone.Oscillator(values.getIntervalFreq(), "sine").connect(imult, 0, 1)



function playBoth() {root.start(); interval.start()}

function stopBoth() {root.stop(); interval.stop()}

function changeInterval(){
  interval.set("frequency", values.getIntervalFreq())
  $("#info").html("resulting frequencies:<br>root = " + values.root.freq.toFixed(2) + "Hz<br>interval = " + values.interval.dividend +"/"+ values.interval.divisor + " * root = " + interval.frequency.value.toFixed(2) + "Hz")
}

function changeRoot(){
  root.set("frequency", values.root.freq)
  changeInterval()
}

function toggleOscillators(v){
  if(this.object.freq)
    if(v) root.start(); else root.stop()
  else
    if(v) interval.start(); else interval.stop()

}

function initUI(){
  // $("<div>", {class : "osc", html:"aaa"})
  var gui = new dat.GUI({autoPlace:false})
  gui.add(values.root, "freq", 1, 1000).onChange(changeRoot).name("root frequency in Hertz").step(0.01)
  gui.add(values.root, "listen").onChange(toggleOscillators).name("listen to root oscillator")
  gui.add(values.interval, "listen").onChange(toggleOscillators).name("listen to interval oscillator")
  gui.add(values.interval, "dividend", 1, 88).onChange(changeInterval).step(0.01).name("dividend for interval's ratio")
  gui.add(values.interval, "divisor", 1, 88).onChange(changeInterval).step(0.01).name("divisor for interval's ratio")
  gui.add(values, "volume", -64, 0).onChange(v=>Tone.Master.set("volume", v)).step(0.01)

  $("#main").append(gui.domElement)

}

initUI()
// playBoth()
changeRoot()

function setup(){
  createCanvas(512, 100).parent("canvas")
}

function draw(){
  background(0)

  ws = waveform.getValue()
  noFill()
  stroke(255)
  beginShape()
  for(var i= 0; i<ws.length; i++){
    vertex(i, height * 0.5 + ws[i] * height * 0.5)
  }
  endShape()
}
